<?php
namespace App\Bitm\SEIP1292\ProfilePicture;
use App\Bitm\SEIP1292\Message\Message;
use App\Bitm\SEIP1292\Utility\Utility;


class ImageUploader{
    public $id="";
    public $name="";
    public $image_name="";
    public $conn;


    public function __construct(){
        $this->conn= mysqli_connect("localhost","root","","atomicprojectb20") or die("Database connection establish failed");
    }

    public function prepare($data=Array())
    {
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("image", $data)) {
            $this->image_name = $data['image'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }


    public function store(){
        $query="INSERT INTO `atomicprojectb20`.`profilepicture` (`name`, `images`) VALUES ('{$this->name}', '{$this->image_name}')";
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect('index.php');
        }
        else {
            Message::message("Data has not been stored successfully");
            Utility::redirect('index.php');
        }
    }

    public  function index(){
        $_allInfo=array();
        $query= "SELECT * FROM `profilepicture`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allInfo[]=$row;
        }
        return $_allInfo;
    }

    public function view(){
        $query="SELECT * FROM `profilepicture` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }

    public function update(){
        if(!empty($this->image_name)) {
            $query = "UPDATE `atomicprojectb20`.`profilepicture` SET `name` = '{$this->name}', `images` = '{$this->image_name}' WHERE `profilepicture`.`id` =" . $this->id;
        }else{
            $query = "UPDATE `atomicprojectb20`.`profilepicture` SET `name` = '{$this->name}' WHERE `profilepicture`.`id` =" . $this->id;
        }
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect('index.php');
        }
        else {
            Message::message("Data has not been updated successfully");
            Utility::redirect('index.php');
        }
    }

    public function delete(){
        $query= "DELETE FROM `atomicprojectb20`.`profilepicture` WHERE `profilepicture`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect('index.php');
        }
        else {
            Message::message("Data has not been deleted successfully");
            Utility::redirect('index.php');
        }

    }







}
